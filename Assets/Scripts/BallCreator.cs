﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BallCreator : MonoBehaviour
{
    public GameObject prefab;
    
    void Start()
    {
        Invoke("CreateBalls", Common.Random.Next(1, 4) / 3.0f);
    }
    

    void CreateBalls()
    {
        //generate balls by random X
        Instantiate(prefab, new Vector2(Common.Random.Next(-8, 8), -6), Quaternion.identity);

        //from 0.5 to 2 sec
        Invoke("CreateBalls", (Common.Random.Next(1, 200) / 100.0f));

    }
}
