﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Ball : MonoBehaviour
{
    public float movementSpeed;
    public Canvas Canvas;
    public int Score;
    
    void Start()
    {
    }
    
    void Update()
    {
        //move ball up
        transform.Translate(Vector2.up * movementSpeed * Time.deltaTime * Common.MovementIndex);

        //delete uncatched balls
        if (transform.position.y > 5.5f)
        {
            Destroy(gameObject);
        }
    }
    void OnMouseDown()
    {
        //increase the score
        Canvas = FindObjectOfType<Canvas>();
        Canvas.GetComponent<Canvas>().GetComponent<Counter>().ChangeScore(Score);
        Destroy(transform.gameObject);
    }
}

