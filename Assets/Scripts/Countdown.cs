﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Linq;

public class Countdown : MonoBehaviour
{
    public Text TimerText;
    public int SecondsLeft;

    private TimeSpan timerSpan;
   
    void Start()
    {
        timerSpan = TimeSpan.FromSeconds(SecondsLeft);
    }

    void Update()
    {
        //show timer
        timerSpan = timerSpan.Subtract(TimeSpan.FromSeconds(Time.deltaTime));
        TimerText.text = timerSpan.ToString(@"mm\:ss");

        //game over
        if (timerSpan.TotalSeconds <= 0)
        {
            GameObject.FindGameObjectsWithTag("ball").ToList().ForEach(x => Destroy(x));
            Time.timeScale = 0;
        }
        
    }
}
