﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Counter : MonoBehaviour
{
    public Text Score;

    void Start()
    {
        Score.text = 0.ToString();
    }


    void Update()
    {
        
    }

    public void ChangeScore(int number)
    {
        Score.text = (int.Parse(Score.text) + number).ToString();
    }
}
